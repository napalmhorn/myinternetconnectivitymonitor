/**
 * My Internet Connectivity Monitor 2.0
 * Delilah Ellison
 * 
 */
package org.bitbucket.napalmhorn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author delilah
 *
 */
public class ConnDatabase {
	// This class handles all database transactions
	private String JDBC_DRIVER;
	private String DB_URL;
	private String username;
	private String tableName;
	private String password;
	private static Connection conn = null;

	public ConnDatabase(String jDBC_DRIVER, String dB_URL, String username, String password) {
		// Load settings for the database.
		try {
			Connection c = new com.mysql.jdbc.Driver().connect("jdbc:mysql://localhost/test?user=root&password=root", null);
		} catch (SQLException e) {
			// This should always fail but it forces the included
		}
		JDBC_DRIVER = jDBC_DRIVER;
		String pattern = "(.*)/(.*)";
		Pattern r = Pattern.compile(pattern);
		Matcher match = r.matcher(dB_URL);
		if (match.find()) {
			DB_URL = "jdbc:" + match.group(1);
			tableName = match.group(2);
		} else {
			DB_URL = "jdbc:" + dB_URL;
		}
		
		this.username = username;
		this.password = password;
	}


	private void openConnection() {
		// creates an SQL connection
		System.out.println("Connecting to database..." + DB_URL + " table:" + tableName);
		try {
			Class.forName(JDBC_DRIVER);
			conn = (Connection) DriverManager.getConnection(DB_URL, username, password);
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("unable to connect to database:" + e);
			System.exit(1);
		}
	}

	public void addRow(String netloc, boolean success, int wait) {
		if (conn == null) {
			openConnection();
		}
		try {
			String sql = "INSERT INTO " + tableName + " (netloc, success, response_time) VALUES (?, ?, ?)";
			PreparedStatement preparedStatement = (PreparedStatement) conn.prepareStatement(sql);
			preparedStatement.setString(1, netloc);
			preparedStatement.setBoolean(2, success);
			preparedStatement.setInt(3, wait);
			try {
				preparedStatement.executeUpdate();
			} catch (Exception e) {
				Thread.sleep(1000);
				preparedStatement.executeUpdate();
			}

		} catch (SQLException | InterruptedException e) {
			System.out.println("Database write failed:" + e);

		}
	}

}
