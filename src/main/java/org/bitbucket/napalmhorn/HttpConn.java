package org.bitbucket.napalmhorn;

/*
 * My Internet Connectivity Monitor 2.0
 * Delilah Ellison
 * Based on: 
 * UserInterface.java class
 * Genc Alikaj - August 2013
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HttpConn implements Runnable {

	private byte runFlag = 0;
	private ConnDatabase db;

	private String lastStatus = null;
	private String nextLastStatus = null;
	private byte fileLocked = 0;

	String objectName = "Primary";

	Disconnections disconnectionObject;

	private String address;
	private int frequency;

	// using a custom constructor that assigns a name to this object
	// and brings a reference to the Disconnections class
	public HttpConn(String name, Disconnections dis) {

		objectName = name;
		disconnectionObject = dis;
	}

	// run method required for implementing Runnable interface.
	// The loop begins when the thread starts (this method is the first thing
	// that runs). \
	// Process depends on byte flag at the top of the class.
	public void run() {
		while (runFlag == 0) {
			StartTest();
		}
	}

	// mutator that sets flag value in order to start or stop test loop
	public void setFlag(byte x) {
		runFlag = x;
	}

	// web address mutator
	public void setAddress(String address) {
		this.address = address;
		if (App.SAVETODATABASE.isSelected()) {
			db = new ConnDatabase(App.JDBCDRIVER.getText(), App.JDBCADDRESS.getText(), App.JDBCUSER.getText(),
					App.JDBCPASS.getText());
		}
	}

	// frequency mutator
	public void setFrequency(int f) {
		if (f < 1) {
			f = 1;
		}
		frequency = f;
	}

	@SuppressWarnings("null")
	public void StartTest() {

		try {
			long start = System.currentTimeMillis();
			// write or update the automatic log file if the option is selected

			// define URL and try to connect to server
			URL url = new URL(address);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// current time saved in a String
			String date = String.format("%ta %<tb %<td %<tT", new Date());

			// The following object is used to temporarily hold server response
			// data (if connection is successful).
			// This is the key that determines whether the connection was
			// successful or not.
			// If the server response data is successfully retrieved, internet
			// connectivity is assumed to be good.
			// If server response data cannot be retrieved for any reason,
			// internet connectivity is assumed to be lost.
			// When data is not retrieved the method throws an exeption.
			@SuppressWarnings("unused")
			Object testObject = connection.getContent();
			long response_time = System.currentTimeMillis() - start;
			// send result to the output textbox on the main UserInterface class
			App.OUTPUT.append(
					date + "\t" + address + "\t" + "OK" + "\t" + response_time + System.getProperty("line.separator"));
			App.OUTPUT.setCaretPosition(App.OUTPUT.getDocument().getLength());

			// free resources
			connection.disconnect();
			connection = null;
			testObject = null;
			url = null;

			// announce to the disconnection flag that the connection was
			// successful
			if (objectName == "Primary") {
				App.DISCONNECTEDPRIMARYSITE = 0;
			} else {
				App.DISCONNECTEDSECONDARYSITE = 0;
			}

			// perform a disconnection counter check
			disconnectionObject.run();

			// write or update the automatic log file if the option is selected
			AutoWriteLogFile();

			// wait a few seconds before repeating the process. The waiting time
			// is defined by the user (in the "frequency" field)
			try {
				Thread.sleep(frequency * 1000);
			} catch (Exception ex) {
				System.out.println("Thread.sleep exception on TRY");
			}

		} catch (Exception e) {

			// write or update the automatic log file if the option is selected
			AutoWriteLogFile();

			// current time saved in a String
			String date = String.format("%ta %<tb %<td %<tT", new Date());

			// send result to the output textbox on the main UserInterface class
			App.OUTPUT.append(date + "\t" + address + "\t" + "NOT CONNECTED" + System.getProperty("line.separator"));
			if (App.SAVETODATABASE.isSelected()) {
				db.addRow(address, false, (Integer) null);
			}
			App.OUTPUT.setCaretPosition(App.OUTPUT.getDocument().getLength());

			// announce to the disconnection flag that the connection was
			// unsuccessful
			if (objectName == "Primary") {
				App.DISCONNECTEDPRIMARYSITE = 1;
			} else {
				App.DISCONNECTEDSECONDARYSITE = 1;
			}

			// perform a disconnection counter check
			disconnectionObject.clearAudioFile();
			disconnectionObject.run();

			// write or update the automatic log file if the option is selected
			AutoWriteLogFile();

			// wait a few seconds before repeating the process. The waiting time
			// is defined by the user (in the "frequency" field)
			try {
				Thread.sleep(frequency * 1000);
			} catch (Exception ex) {
				System.out.println("Thread.sleep exception on CATCH");
			}
		}
	}

	public void AutoWriteLogFile() {
		if (fileLocked != 1 && App.CHKAUTOSAVE.isSelected()) {
			String line = App.OUTPUT.getText();
			try {
				// get path from textbox, and then add escape characters to
				// slashes to avoid errors.
				String rawPath = App.TXTAUTOSAVEPATH.getText();
				String fixedPath = rawPath.replace("\\", "\\\\");

				File logFile = new File(fixedPath);

				// create path if missing
				logFile.getParentFile().mkdirs();

				// write contents of Output textarea to file
				BufferedWriter bw = new BufferedWriter(new FileWriter(logFile));

				bw.write(line);
				// save to database also

				// close filewriter
				bw.flush();
				bw.close();

			} catch (Exception e) {
				App.OUTPUT.append(System.getProperty("line.separator")
						+ "Cannot automatically create or write to file. Please check path."
						+ System.getProperty("line.separator"));
				App.OUTPUT
						.append("It's possible that access to the selected location is denied. Try using a different path."
								+ System.getProperty("line.separator") + System.getProperty("line.separator"));
				e.printStackTrace();
				// stop trying to automatically write to file if it's not
				// accessible
				fileLocked = 1;
			}
		}
		if (App.SAVETODATABASE.isSelected()) {
			String[] lines = App.OUTPUT.getText().split("\n");
			String line = lines[lines.length - 1];
			String pattern = ".*\\s(http\\S+)\\s+(.*)\\s(\\d+)";
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(line);
			// avid rewriting the same data
			if (m.find() && (false == (line.equals(lastStatus)) || line.equals(nextLastStatus))) {

				String netloc = m.group(1);
				boolean success;
				if (m.group(2).equals("OK")) {
					success = true;
				} else {
					success = false;
				}
				int response_time = Integer.valueOf(m.group(3));
				db.addRow(netloc, success, response_time);
				nextLastStatus = lastStatus;
				lastStatus = line;
			} else {
				System.out.printf("skipping duplicate:%s:%s:%s\n", line, nextLastStatus, lastStatus);
			}

		}
	}

}
