package org.bitbucket.napalmhorn;

/* 
 * My Internet Connectivity Monitor 2.0
 * Delilah Ellison
 * Based on: 
 * Disconnections.java class
 * Genc Alikaj - August 2013
 */

import java.awt.Color;
import java.io.File;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Disconnections {

	// audio file to play when disconnected
	Clip clpDisconnected;

	public synchronized void run() {

		// if both websites are disconnected increase counter by one
		if (App.CHKSECONDARYADDRESS.isSelected() == true & App.DISCONNECTEDPRIMARYSITE == (byte) 1
				& App.DISCONNECTEDSECONDARYSITE == (byte) 1 & App.SAMEDISCONNECTION == (byte) 0) {

			App.SAMEDISCONNECTION = (byte) 1;
			App.DISCONNECTIONCOUNTER++;
			App.LBLDISCONNECTIONCOUNTER.setText(Integer.toString(App.DISCONNECTIONCOUNTER));
			App.LBLDISCONNECTIONCOUNTER.setForeground(Color.RED);

			// play sound when disconnected (when counter goes up by one)
			if (App.CHKPLAYSOUND.isSelected() == true) {
				try {
					Clip clpDisconnected = AudioSystem.getClip();
					clpDisconnected.open(AudioSystem.getAudioInputStream(new File("Disconnected.wav")));
					clpDisconnected.start();
				} catch (Exception e) {
					App.OUTPUT.append("(Information) Cannot play sound" + System.getProperty("line.separator"));
					App.OUTPUT.setCaretPosition(App.OUTPUT.getDocument().getLength());
				}
			}
		}

		// increase disconnection counter when ONLY one website is being
		// monitored
		if (App.CHKSECONDARYADDRESS.isSelected() == false & App.DISCONNECTEDPRIMARYSITE == (byte) 1
				& App.SAMEDISCONNECTION == (byte) 0) {

			App.SAMEDISCONNECTION = (byte) 1;
			App.DISCONNECTIONCOUNTER++;
			App.LBLDISCONNECTIONCOUNTER.setText(Integer.toString(App.DISCONNECTIONCOUNTER));
			App.LBLDISCONNECTIONCOUNTER.setForeground(Color.RED);

			// play sound when disconnected (when counter goes up by one)
			if (App.CHKPLAYSOUND.isSelected() == true) {
				try {
					Clip clpDisconnected = AudioSystem.getClip();
					clpDisconnected.open(AudioSystem.getAudioInputStream(new File("Disconnected.wav")));
					clpDisconnected.start();
				} catch (Exception e) {
					App.OUTPUT.append("(Information) Cannot play sound" + System.getProperty("line.separator"));
					App.OUTPUT.setCaretPosition(App.OUTPUT.getDocument().getLength());
				}
			}
		}

		// the following two conditionals tell the program that connection has
		// been re-established.
		if (App.CHKSECONDARYADDRESS.isSelected() == true
				& (App.DISCONNECTEDPRIMARYSITE == (byte) 0 || App.DISCONNECTEDSECONDARYSITE == (byte) 0)
				& App.SAMEDISCONNECTION == (byte) 1) {

			App.SAMEDISCONNECTION = (byte) 0;
		}

		if (App.CHKSECONDARYADDRESS.isSelected() == false & App.DISCONNECTEDPRIMARYSITE == (byte) 0
				& App.SAMEDISCONNECTION == (byte) 1) {

			App.SAMEDISCONNECTION = (byte) 0;
		}

		// loop disconnection sound if option is selected.
		if (App.CHKPLAYSOUNDLOOP.isSelected() == true & App.SAMEDISCONNECTION == (byte) 1) {

			try {
				clpDisconnected = AudioSystem.getClip();
				clpDisconnected.open(AudioSystem.getAudioInputStream(new File("Disconnected.wav")));
				clpDisconnected.start();
			} catch (Exception e) {
				App.OUTPUT.append("(Information) Cannot play sound" + System.getProperty("line.separator"));
				App.OUTPUT.setCaretPosition(App.OUTPUT.getDocument().getLength());
			}
		}

	}

	// free clip memory resources
	public synchronized void clearAudioFile() {
		clpDisconnected = null;
		System.gc();
	}

}
